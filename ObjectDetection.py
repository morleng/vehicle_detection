import cv2
import numpy as np


class OpencvYOLO(object):
    _defaults = {
        # Initialize the parameters
        "cfg": "./model_data/plate/yolov3-tiny_lp.cfg",
        "weights": "./model_data/plate/lp_tiny.weights",
        "objnames": "./model_data/plate/lp.names",
        "score": 0.5,  # Confidence threshold,
        "nms": 0.6,  # Non-maximum suppression threshold
        "inpWidth": 416,  # Width of network's input image
        "inpHeight": 416  # Height of network's input image
    }

    @classmethod
    def get_defaults(cls, n):
        if n in cls._defaults:
            return cls._defaults[n]
        else:
            return "Unrecognized attribute name '" + n + "'"

    def __init__(self, **kwargs):
        print(kwargs)
        self.__dict__.update(self._defaults)  # set up default values
        self.__dict__.update(kwargs)  # and update with user overrides
        self.frame = None
        self.classes = None
        with open(self.objnames, 'rt') as f:
            self.classes = f.read().rstrip('\n').split('\n')

        dnn = cv2.dnn.readNetFromDarknet(self.cfg, self.weights)
        dnn.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
        # dnn.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL)
        dnn.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

        self.net = dnn
        print("Loaded model ..")

    def setScore(self, score=0.5):
        self.score = score

    def setNMS(self, nms=0.6):
        self.nms = nms

    # Get the names of the output layers
    def getOutputsNames(self, net):
        # Get the names of all the layers in the network
        layersNames = net.getLayerNames()
        # Get the names of the output layers, i.e. the layers with unconnected outputs
        return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # Remove the bounding boxes with low confidence using non-maxima suppression
    def postprocess(self, frame, outs, labelWant, drawBox):
        frameHeight = frame.shape[0]
        frameWidth = frame.shape[1]

        self.frame = frame
        classIds = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                classId = np.argmax(scores)
                # print(classId)
                confidence = scores[classId]
                label = self.classes[classId]
                if (labelWant == "" or (label in labelWant)) and (confidence > self.score):
                    center_x = int(detection[0] * frameWidth)
                    center_y = int(detection[1] * frameHeight)
                    width = int(detection[2] * frameWidth)
                    height = int(detection[3] * frameHeight)
                    left = int(center_x - width / 2)
                    top = int(center_y - height / 2)
                    classIds.append(classId)
                    confidences.append(float(confidence))
                    boxes.append((left, top, width, height))

        # Perform non maximum suppression to eliminate redundant overlapping boxes with
        # lower confidences.
        indices = cv2.dnn.NMSBoxes(boxes, confidences, self.score, self.nms)
        self.indices = indices

        for i in indices:
            i = i[0]
            box = boxes[i]
            left = box[0]
            top = box[1]
            width = box[2]
            height = box[3]

            if (drawBox == True):
                self.drawPred(frame, classIds[i], confidences[i], left, top, left + width, top + height)

        self.bbox = boxes
        self.classIds = classIds
        self.scores = confidences

    # Draw the predicted bounding box
    def drawPred(self, frame, classId, conf, left, top, right, bottom):
        # Draw a bounding box.
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 3)

        label = '%.2f' % conf

        # Get the label for the class name and its confidence
        if self.classes:
            assert (classId < len(self.classes))
            label = '%s:%s' % (self.classes[classId], label)

        # Display the label at the top of the bounding box
        labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
        top = max(top, labelSize[1])
        ft = cv2.freetype.createFreeType2()
        ft.loadFontData(fontFileName='./fonts/THSarabun.ttf', id=0)
        ft.putText(img=frame, text=label, org=(left, top), fontHeight=40, color=(0, 0, 255), thickness=-1,
                   line_type=cv2.LINE_AA, bottomLeftOrigin=True)
        # cv2.putText(frame, label, (left, top), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

    # def getObject(self, frame, labelWant=("plate"), drawBox=False):
    #     blob = cv2.dnn.blobFromImage(frame, 1 / 255, (self.inpWidth, self.inpHeight), [0, 0, 0], 1, crop=False)
    #     # Sets the input to the network
    #     net = self.net
    #     net.setInput(blob)
    #     # Runs the forward pass to get output of the output layers
    #     outs = net.forward(self.getOutputsNames(net))
    #     # Remove the bounding boxes with low confidence
    #     self.postprocess(frame, outs, labelWant, drawBox)
    #     self.objCounts = len(self.indices)
    #     # Put efficiency information. The function getPerfProfile returns the
    #     # overall time for inference(t) and the timings for each of the layers(in layersTimes)
    #     t, _ = net.getPerfProfile()

        # return frame
        # label = 'Inference time: %.2f ms' % (t * 1000.0 / cv2.getTickFrequency())

    def getObject(self, frame, labelWant=("plate"), drawBox=False):
        detected_objects = []
        blob = cv2.dnn.blobFromImage(frame, 1 / 255, (self.inpWidth, self.inpHeight), [0, 0, 0], 1, crop=False)
        # Sets the input to the network
        net = self.net
        net.setInput(blob)
        # Runs the forward pass to get output of the output layers
        outs = net.forward(self.getOutputsNames(net))
        # Remove the bounding boxes with low confidence
        self.postprocess(frame, outs, labelWant, drawBox)
        self.objCounts = len(self.indices)
        # Put efficiency information. The function getPerfProfile returns the
        # overall time for inference(t) and the timings for each of the layers(in layersTimes)
        t, _ = net.getPerfProfile()

        for i in self.indices:
            dict_objects = {}
            i = i[0]
            box = self.bbox[i]
            # replace negative values with 1
            box = [0 if k < 0 else k for k in box]
            left = box[0]
            top = box[1]
            width = box[2]
            height = box[3]
            classes = self.classes
            dict_objects.update({"label": classes[self.classIds[i]]})
            dict_objects.update({"score": self.scores[i]})
            # dict_objects.update({"location": {"x1": left, "y1":top, "x2":left + width, "y2": top + height}})
            dict_objects.update({"location": {
                    "x": left, 
                    "y": top, 
                    "w": width, 
                    "h": height}
                    })

            # print(self.frame)
            # cropped_img = self.crop_image(top, height, left, width)
            # retval, buffer = cv2.imencode('.jpg', cropped_img)
            # pic_str = base64.b64encode(buffer)
            # pic_str = pic_str.decode()
            # dict_objects.update({"cropped_image": pic_str})
            detected_objects.append(dict_objects)
            # print("Label:{}, score:{}, left:{}, top:{}, right:{}, bottom:{}".format(classes[self.classIds[i]],
            #                                                                         self.scores[i], left, top,
            #                                                                         left + width, top + height))
        return detected_objects

    # def crop_image(self, top, height, left, width):
    #     cropped = self.frame[top:height, left:width, :]
    #     return cropped

